package cn.bootx.platform.daxpay.service.core.order.reconcile.service;

import cn.bootx.platform.daxpay.service.core.order.reconcile.dao.PayReconcileDiffRecordManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 *
 * @author xxm
 * @since 2024/2/29
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PayReconcileDiffRecordService {
    private final PayReconcileDiffRecordManager recordManager;


}
